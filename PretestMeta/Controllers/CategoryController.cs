﻿using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PretestMeta.Model;
using PretestMeta.Service.Interface;
using System.Data;

namespace PretestMeta.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : Controller
    {
        private readonly ICategory _authentication;
        public CategoryController(ICategory authentication)
        {
            _authentication = authentication;
        }

        [HttpGet("CategoryList")]
        [Authorize(Roles = "Admin")]
        public IActionResult getCategory()
        {
            var result = _authentication.getCategoryList<ModelDocument_Category>();

            return Ok(result);
        }

        [HttpGet("CategoryListbyID")]
        [Authorize(Roles = "Admin")]
        public IActionResult getCategorybyID(int id)
        {
            var result = _authentication.getCategoryListbyID<ModelDocument_Category>(id);

            return Ok(result);
        }

        [HttpPost("Create")]
        [Authorize(Roles = "Admin")]
        public IActionResult Register([System.Web.Http.FromBody] ModelDocument_Category category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("name", category.Name, DbType.String);
            dp_param.Add("iduser", category.CreatedBy, DbType.Int32);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocument_Category>("sp_CreateDocument_Category", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = category, code = 200, message = "Success" });
            }

            return BadRequest(result);
        }

        [HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([System.Web.Http.FromBody] ModelDocument_Category category, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("name", category.Name, DbType.String);
            dp_param.Add("iduser", category.CreatedBy, DbType.Int32);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocument_Category>("sp_updateDocument_Category", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = category, code = 200, message = "Success" });
            }

            return BadRequest(result);
        }

        [Microsoft.AspNetCore.Mvc.HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocument_Category>("sp_deleteDocument_Category", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }
    }
}
