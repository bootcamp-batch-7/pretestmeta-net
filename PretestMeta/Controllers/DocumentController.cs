﻿using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PretestMeta.Model;
using PretestMeta.Service.Interface;
using System.Data;
using System.Reflection.Metadata;

namespace PretestMeta.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentController : Controller
    {
        private readonly IDocument _authentication;
        public DocumentController(IDocument authentication)
        {
            _authentication = authentication;
        }

        [HttpGet("DocumentList")]
        [Authorize(Roles = "Admin")]
        public IActionResult getDocument()
        {
            var result = _authentication.getDocumentList<ModelView_Document>();

            return Ok(result);
        }

        [HttpGet("DocumentListbyID")]
        [Authorize(Roles = "Admin")]
        public IActionResult getCompanybyID(int id)
        {
            var result = _authentication.getDocumentListbyID<ModelView_Document>(id);

            return Ok(result);
        }

        [HttpPost("Create")]
        [Authorize(Roles = "Admin")]
        public IActionResult Register([System.Web.Http.FromBody] ModelDocument document)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("idcompany", document.IDCompany, DbType.String);
            dp_param.Add("idcategory", document.IDCompany, DbType.String);
            dp_param.Add("name", document.Name, DbType.String);
            dp_param.Add("description", document.Description, DbType.String);
            dp_param.Add("flag", document.Flag, DbType.Int32);
            dp_param.Add("iduser", document.CreatedBy, DbType.Int32);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocument>("sp_createDocument", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = document, code = 200, message = "Success" });
            }

            return BadRequest(result);
        }

        [HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([System.Web.Http.FromBody] ModelDocument document, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("idcompany", document.IDCompany, DbType.String);
            dp_param.Add("idcategory", document.IDCompany, DbType.String);
            dp_param.Add("name", document.Name, DbType.String);
            dp_param.Add("description", document.Description, DbType.String);
            dp_param.Add("flag", document.Flag, DbType.Int32);
            dp_param.Add("iduser", document.CreatedBy, DbType.Int32);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelCompany>("sp_updateDocument", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = document, code = 200, message = "Success" });
            }

            return BadRequest(result);
        }

        [Microsoft.AspNetCore.Mvc.HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocument>("sp_deleteDocument", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }
    }
}
