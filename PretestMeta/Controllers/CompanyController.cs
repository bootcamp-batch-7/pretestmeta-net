﻿using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PretestMeta.Model;
using PretestMeta.Service.Interface;
using System.Data;

namespace PretestMeta.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : Controller
    {
        private readonly ICompany _authentication;
        public CompanyController(ICompany authentication)
        {
            _authentication = authentication;
        }

        [HttpGet("CompanyList")]
        [Authorize(Roles = "Admin")]
        public IActionResult getCompany()
        {
            var result = _authentication.getCompanyList<ModelCompany>();

            return Ok(result);
        }

        [HttpGet("CompanyListbyID")]
        [Authorize(Roles = "Admin")]
        public IActionResult getCompanybyID(int id)
        {
            var result = _authentication.getCompanyListbyID<ModelCompany>(id);

            return Ok(result);
        }

        [HttpPost("Create")]
        [Authorize(Roles = "Admin")]
        public IActionResult Register([System.Web.Http.FromBody] ModelCompany company)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("name", company.Name, DbType.String);
            dp_param.Add("address", company.Address, DbType.String);
            dp_param.Add("email", company.Email, DbType.String);
            dp_param.Add("telephone", company.Telephone, DbType.String);
            dp_param.Add("flag", company.Flag, DbType.Int32);
            dp_param.Add("iduser", company.CreatedBy, DbType.Int32);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelCompany>("sp_createCompany", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = company, code = 200, message = "Success" });
            }

            return BadRequest(result);
        }

        [HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([System.Web.Http.FromBody] ModelCompany company, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("name", company.Name, DbType.String);
            dp_param.Add("address", company.Address, DbType.String);
            dp_param.Add("email", company.Email, DbType.String);
            dp_param.Add("telephone", company.Telephone, DbType.String);
            dp_param.Add("flag", company.Flag, DbType.Int32);
            dp_param.Add("iduser", company.CreatedBy, DbType.Int32);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelCompany>("sp_updateCompany", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = company, code = 200, message = "Success" });
            }

            return BadRequest(result);
        }

        [Microsoft.AspNetCore.Mvc.HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelUser>("sp_deleteCompany", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }
    }
}
