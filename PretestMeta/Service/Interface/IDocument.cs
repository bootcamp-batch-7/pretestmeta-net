﻿using Dapper;
using PretestMeta.Model;
using System.Collections.Generic;

namespace PretestMeta.Service.Interface
{
    public interface IDocument
    {
        Response<T> Execute_Command<T>(string query, DynamicParameters sp_params);

        Response<List<T>> getDocumentList<T>();
        Response<List<T>> getDocumentListbyID<T>(int ID);
    }
}
