﻿using Dapper;
using PretestMeta.Model;
using System.Collections.Generic;

namespace PretestMeta.Service.Interface
{
    public interface IPosition
    {
        Response<T> Execute_Command<T>(string query, DynamicParameters sp_params);
        Response<List<T>> getPositionList<T>();
        Response<List<T>> getPositionListbyID<T>(int ID);
    }
}
