﻿using Dapper;
using PretestMeta.Model;
using System.Collections.Generic;

namespace PretestMeta.Service.Interface
{
    public interface ICompany
    {
        Response<T> Execute_Command<T>(string query, DynamicParameters sp_params);
        Response<List<T>> getCompanyList<T>();
        Response<List<T>> getCompanyListbyID<T>(int ID);
    }
}
