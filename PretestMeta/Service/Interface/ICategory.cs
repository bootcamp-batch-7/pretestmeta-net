﻿using Dapper;
using PretestMeta.Model;
using System.Collections.Generic;

namespace PretestMeta.Service.Interface
{
    public interface ICategory
    {
        Response<T> Execute_Command<T>(string query, DynamicParameters sp_params);
        Response<List<T>> getCategoryList<T>();
        Response<List<T>> getCategoryListbyID<T>(int ID);
    }
}
