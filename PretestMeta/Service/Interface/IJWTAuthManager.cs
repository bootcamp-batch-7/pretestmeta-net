﻿using Dapper;
using PretestMeta.Model;
using System.Collections.Generic;

namespace PretestMeta.Service.Interface
{
    public interface IJWTAuthManager
    {
        Response<string> GenerateJWT(ModelUser user);
        Response<T> Execute_Command<T>(string query, DynamicParameters sp_params);
        Response<List<T>> getUserList<T>();
        Response<List<T>> getUserListbyID<T>(int ID);
    }
}
