﻿using Dapper;
using PretestMeta.Model;
using PretestMeta.Service.Interface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace PretestMeta.Service.Repository
{
    public class Position : IPosition
    {
        private readonly IConfiguration _configuration;
        public Position(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public Response<T> Execute_Command<T>(string query, DynamicParameters sp_params)
        {
            throw new System.NotImplementedException();
        }

        public Response<List<T>> getPositionList<T>()
        {
            Response<List<T>> response = new Response<List<T>>();
            using IDbConnection db = new SqlConnection(_configuration.GetConnectionString("default"));
            string query = "select * from TBPosition";

            try
            {
                response.Data = db.Query<T>(query, null, commandType: CommandType.Text).ToList();
                response.Code = 200;
                response.Message = "Success";
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
            }

            return response;
        }

        public Response<List<T>> getPositionListbyID<T>(int ID)
        {
            Response<List<T>> response = new Response<List<T>>();
            using IDbConnection db = new SqlConnection(_configuration.GetConnectionString("default"));
            string query = "select * from TBPosition where id = " + ID;

            try
            {
                response.Data = db.Query<T>(query, null, commandType: CommandType.Text).ToList();
                response.Code = 200;
                response.Message = "Success";
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
            }

            return response;
        }
    }
}
