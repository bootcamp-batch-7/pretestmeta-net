﻿using Dapper;
using Microsoft.Extensions.Configuration;
using PretestMeta.Model;
using PretestMeta.Service.Interface;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System;
using System.Linq;

namespace PretestMeta.Service.Repository
{
    public class Category : ICategory
    {
        private readonly IConfiguration _configuration;
        public Category(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public Response<T> Execute_Command<T>(string query, DynamicParameters sp_params)
        {
            Response<T> response = new Response<T>();

            using (IDbConnection dbConnection = new SqlConnection(_configuration.GetConnectionString("default")))
            {
                if (dbConnection.State == ConnectionState.Closed)
                    dbConnection.Open();

                using var transaction = dbConnection.BeginTransaction();

                try
                {
                    response.Data = dbConnection.Query<T>(query, sp_params, commandType: CommandType.StoredProcedure, transaction: transaction).FirstOrDefault();
                    response.Code = sp_params.Get<int>("retVal"); //get output parameter value
                    response.Message = "Success";
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    response.Code = 500;
                    response.Message = ex.Message;
                }
            }

            return response;
        }

        public Response<List<T>> getCategoryList<T>()
        {
            Response<List<T>> response = new Response<List<T>>();
            using IDbConnection db = new SqlConnection(_configuration.GetConnectionString("default"));
            string query = "select * from TBDocument_Category";

            try
            {
                response.Data = db.Query<T>(query, null, commandType: CommandType.Text).ToList();
                response.Code = 200;
                response.Message = "Success";
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
            }

            return response;
        }

        public Response<List<T>> getCategoryListbyID<T>(int ID)
        {
            Response<List<T>> response = new Response<List<T>>();
            using IDbConnection db = new SqlConnection(_configuration.GetConnectionString("default"));
            string query = "select * from TBDocument_Category where id = " + ID;

            try
            {
                response.Data = db.Query<T>(query, null, commandType: CommandType.Text).ToList();
                response.Code = 200;
                response.Message = "Success";
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
            }

            return response;
        }
    }
}
