﻿using System.ComponentModel.DataAnnotations;

namespace PretestMeta.Model
{
    public class ModelDocument_Category
    {
        [Required]
        public string? Name { get; set; }

        public int? CreatedBy { get; set; }
    }
}
