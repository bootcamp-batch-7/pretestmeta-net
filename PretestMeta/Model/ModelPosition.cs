﻿using System.ComponentModel.DataAnnotations;
using System;

namespace PretestMeta.Model
{
    public class ModelPosition
    {
        [Required]
        public string? Name { get; set; }

        public int? CreatedBy { get; set; }
    }
}
