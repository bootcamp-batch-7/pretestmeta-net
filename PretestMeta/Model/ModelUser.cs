﻿using System.ComponentModel.DataAnnotations;
using System;

namespace PretestMeta.Model
{
    public class ModelUser
    {
        [Required]
        public int? IDCompany { get; set; }

        [Required]
        public int? IDPosition { get; set; }

        [Required]
        public string? Name { get; set; }

        public string? Address { get; set; }

        [Required]
        public string? Email { get; set; }

        public string? Telephone { get; set; }

        [Required]
        public string? Username { get; set; }

        [Required]
        public string? Password { get; set; }

        [Required]
        public string? Role { get; set; }

        public int? Flag { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;
    }
}
