﻿using System.ComponentModel.DataAnnotations;
using System;

namespace PretestMeta.Model
{
    public class ModelView_User
    {
        public string? Company { get; set; }

        public string? Position { get; set; }

        [Required]
        public string? Name { get; set; }

        public string? Address { get; set; }

        [Required]
        public string? Email { get; set; }

        public string? Telephone { get; set; }

        [Required]
        public string? Username { get; set; }

        [Required]
        public string? Password { get; set; }

        [Required]
        public string? Role { get; set; }

        public int? Flag { get; set; }

        public int? CreatedBy { get; set; }

    }
}
