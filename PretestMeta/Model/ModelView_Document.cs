﻿using System.ComponentModel.DataAnnotations;
using System;

namespace PretestMeta.Model
{
    public class ModelView_Document
    {
        [Required]
        public string? Company { get; set; }

        [Required]
        public string? Category { get; set; }

        [Required]
        public string? Name { get; set; }

        public string? Description { get; set; }

        public int? Flag { get; set; }

        public int? CreatedBy { get; set; }
    }
}
